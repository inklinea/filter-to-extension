#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2023] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Filter To Extension - Extract a Filter Stack From an object and conver to an extension
# Appears under Extensions>Filter To Extension
# The filter stack is saved as a zip file and can be installed as a normal extension
# The installed filter extension then appears under Extensions>Filter Extensions>Name
# An Inkscape 1.2+ extension
# It can be used from the command line ( currently the --batch-process option is
# required for Inkscape 1.2/1.3 )
# To list the action names for installed filter
# Linux inkscape --action-list | grep filter-extn
# Windows inkscapecom.com --action-list | findstr filter-extn
##############################################################################


import inkex

from slugify import slugify

from zipfile import ZipFile

import sys

# Platform Check
################
def os_check(self):
    """
    Check which OS we are using
    :return: OS Name ( windows, linux, macos )
    """
    from sys import platform

    if 'linux' in platform.lower():
        return 'linux'
    elif 'darwin' in platform.lower():
        return 'macos'
    elif 'win' in platform.lower():
        return 'windows'


def get_filter_from_element(self, element):

    if filter_url := element.style.get('filter'):
        filter_id = filter_url[4:-1]
    else:
        inkex.errormsg('No Filter on Object')
        sys.exit()

    current_filter = self.svg.getElementById(filter_id)

    return filter_id, filter_url, current_filter

def check_for_duplicate_extension_id(self, extension_id):

    from inkex import command
    import subprocess

    if os_check(self) == 'windows':

        inkscape_actions = subprocess.run(["c:/program files/inkscape/bin/inkscape.exe", '--action-list'], capture_output=True)
        inkscape_action_list = inkscape_actions.stdout.decode('cp1252')

    else:
        inkscape_action_list = command.inkscape('--action-list')

    inkscape_action_list_actions_only = []

    for item in inkscape_action_list.split('\n'):
        inkscape_action_list_actions_only.append(item.split(':')[0].strip().replace('_', '-'))

    extension_dash_id = extension_id.strip().replace('_', '-')

    if extension_dash_id in inkscape_action_list_actions_only:
        inkex.errormsg('An extension with this ID is already installed in the main program')


def make_filter_svg(self, current_filter):

    # A dummy SVG which contains a copy and paste of a user saved filter
    # Replace <filter>.....</filter> with your saved filter

    filter_svg_text = f"""\
<svg
xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
xmlns="http://www.w3.org/2000/svg"
xmlns:svg="http://www.w3.org/2000/svg">
<defs>
    
{current_filter.tostring().decode('utf-8')}

</defs>
</svg>
"""
    return filter_svg_text

def make_inx_text(self, extension_name):

    extension_name_list = extension_name.split('_')
    menu_entry = ' '.join([x.capitalize() for x in extension_name_list])

    extension_id = f'inklinea.{extension_name}_filter_extn'

    # Lets see if there is already an extension installed with this id.
    if self.options.extension_exists_cb:
        check_for_duplicate_extension_id(self, extension_id)

    inx_text = f"""\
<?xml version="1.0" encoding="UTF-8"?>
<inkscape-extension xmlns="http://www.inkscape.org/namespace/inkscape/extension">
    <name>{menu_entry}</name>
    <id>inklinea.{extension_id}</id>

    <!--  Parameters Here -->

    <effect>
        <object-type>all</object-type>
        <effects-menu>
            <submenu name="Filter Extensions"/>
        </effects-menu>
    </effect>
    <script>
        <command location="inx" interpreter="python">{extension_name}_filter_extn.py</command>
    </script>
</inkscape-extension>
"""
    return inx_text

def make_py_text(self, extension_name, filter_svg_text):

    extension_name_list = extension_name.split('_')
    effect_name = ''.join([x.capitalize() for x in extension_name_list])

    py_text = f"""\
import inkex

from lxml import etree

class {effect_name}(inkex.EffectExtension):

    def add_arguments(self, pars):
        pass
    
    def effect(self):
            
        filter_svg_text = '''{filter_svg_text}'''
        
               # Use etree to encode the filter text to an etree element

        my_filter_svg = etree.fromstring(f'''{filter_svg_text}''')

        # Lets set a custom filter id

        my_filter_id = f'{extension_name}'

        # Check if this filter already exists in defs

        filter_id_list = []
        for _filter in self.svg.defs.xpath('//svg:filter'):
            filter_id_list.append(_filter.get_id())


        if my_filter_id in filter_id_list:
            pass
        else:
            # Append the filter element to the svg defs
            my_filter = my_filter_svg.findall('.//{{http://www.w3.org/2000/svg}}filter')[0]
            # Set the id
            my_filter.set('id', my_filter_id)
            self.svg.defs.append(my_filter)

        # build a url for the id

        my_filter_url = f'url(#{{my_filter_id}})'

        # apply the filter to any selected elements

        for item in self.svg.selected:
            if item.style.get('filter') == my_filter_url:
                item.style.pop('filter')
            else:
                item.style['filter'] = my_filter_url
        
                
if __name__ == '__main__':
    {effect_name}().run()
"""
    return py_text


def create_extension_zipfile(self, inx_string, py_string, extension_name):
    try:
        with ZipFile(f'{self.options.extension_folder}/{extension_name}_filter.zip', 'w') as myzip:

            myzip.writestr(f'{extension_name}_filter/{extension_name}_filter_extn.inx', inx_string)
            myzip.writestr(f'{extension_name}_filter/{extension_name}_filter_extn.py', py_string)
    except:
        inkex.errormsg('Cannot Write to Folder')


class FilterToExtension(inkex.EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--extension_folder", type=str, dest="extension_folder", default=None)

        pars.add_argument("--extension_name_string", type=str, dest="extension_name_string", default='x')

        pars.add_argument("--extension_exists_cb", type=inkex.Boolean, dest="extension_exists_cb", default=False)
    
    def effect(self):
        if len(self.svg.selected) < 1:
            inkex.errormsg('Nothing Selected')
            return
        # Only consider the first selected object and grab the filter on that object.
        filter_id, filter_url, current_filter = get_filter_from_element(self, self.svg.selected[0])

        filter_svg_text = make_filter_svg(self, current_filter)

        extension_name = slugify(self.options.extension_name_string)

        inx_string = make_inx_text(self, extension_name)

        py_string = make_py_text(self, extension_name, filter_svg_text)

        create_extension_zipfile(self, inx_string, py_string, extension_name)

if __name__ == '__main__':
    FilterToExtension().run()
