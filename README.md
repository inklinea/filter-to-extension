##############################################################################
# Filter To Extension - Extract a Filter Stack From an object and conver to an extension
# Appears under Extensions>Filter To Extension
# The filter stack is saved as a zip file and can be installed as a normal extension
# The installed filter extension then appears under Extensions>Filter Extensions>Name
# An Inkscape 1.2+ extension
# It can be used from the command line ( currently the --batch-process option is
# required for Inkscape 1.2/1.3 )
# To list the action names for installed filter
# Linux inkscape --action-list | grep filter-extn
# Windows inkscapecom.com --action-list | findstr filter-extn
##############################################################################
